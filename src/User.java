import java.net.Socket;

public class User {
    private String username;
    private String password;
    private int coins;
    private int scores;
    private boolean wait_for_game;
    private Socket socket;

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public boolean isWait_for_game() {
        return wait_for_game;
    }

    public void setWait_for_game(boolean wait_for_game) {
        this.wait_for_game = wait_for_game;
    }

    public User(String username, String password) {
        setUsername(username);
        setPassword(password);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCoins() {
        return coins;
    }

    public int getScores() {
        return scores;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public void setScores(int scores) {
        this.scores = scores;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

