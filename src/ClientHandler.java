import javax.imageio.IIOException;
import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable{
   protected Socket socket;
   protected DataInputStream dis;
   protected DataOutputStream dos;

    public ClientHandler(Socket socket) throws IOException {
        this.socket = socket;
        this.dis = new DataInputStream(socket.getInputStream());
        this.dos = new DataOutputStream(socket.getOutputStream());
    }

    private void login() throws IOException{
        System.out.println("login");
        boolean repeat = true;
        while (repeat){
            String username = dis.readUTF();
            String password = dis.readUTF();

            if (Server.users_hashmap.containsKey(username)) {
                User user = Server.users_hashmap.get(username);
                if (user.getPassword().equals(password)){
                    dos.writeUTF("ok");
                    repeat=false;
                }
                else
                    dos.writeUTF("error");
            }
            else
                dos.writeUTF("error");



        }


    }
    private void signup() throws IOException{
        System.out.println("signup");
        boolean repeat = true;
        String username="";
        while (repeat){

            username = dis.readUTF();
            if (Server.users_hashmap.containsKey(username)){
                dos.writeUTF("error");
            }else {
                dos.writeUTF("ok");
                repeat=false;
            }

        }

        String password = dis.readUTF();
        User user = new User(username,password);


        Server.users_hashmap.put(username,user);


        File my_user_file = new File(Server.path+"Quiz_of_kings_DB\\"+username+".txt");
        my_user_file.createNewFile();
        PrintWriter writer = new PrintWriter(my_user_file);
        writer.println(password);
        writer.flush();
        writer.close();
    }

    public void change_username() throws Exception{
        System.out.println("change username");
        boolean repeat = true;
        String new_username="";
        String old_username="";
        while (repeat){
            old_username = dis.readUTF();
            new_username = dis.readUTF();
            if (Server.users_hashmap.containsKey(new_username)){
                dos.writeUTF("error");
            }else {
                dos.writeUTF("ok");
                repeat=false;
            }
        }

        User user = Server.users_hashmap.get(old_username);
        Server.users_hashmap.remove(old_username);
        user.setUsername(new_username);
        String password = user.getPassword();
        Server.users_hashmap.put(new_username,user);
        File my_user_file = new File(Server.path+"Quiz_of_kings_DB\\"+old_username+".txt");
        File new_my_user_file = new File(Server.path+"Quiz_of_kings_DB\\"+new_username+".txt");
//        my_user_file.renameTo(new_my_user_file);
        boolean delete =  my_user_file.delete();
        System.out.println(delete);
        if (!delete){
            FileWriter fileWriter = new FileWriter(my_user_file);
            fileWriter.write("****");
        }
        new_my_user_file.createNewFile();
        PrintWriter writer = new PrintWriter(new_my_user_file);
        writer.println(password);
        writer.flush();
        writer.close();

    }

    public void change_password() throws Exception{
        System.out.println("change password");
        String username = dis.readUTF();
        String password = dis.readUTF();
        User user = Server.users_hashmap.get(username);
        user.setPassword(password);
        Server.users_hashmap.remove(username);
        Server.users_hashmap.put(username,user);
        File user_file = new File(Server.path+"Quiz_of_kings_DB\\"+username+".txt");
        user_file.delete();
        File new_user_file = new File(Server.path+"Quiz_of_kings_DB\\"+username+".txt");
        new_user_file.createNewFile();
        PrintWriter writer = new PrintWriter(new_user_file);
        writer.println(password);
        writer.flush();
        writer.close();


    }

    public void new_game() throws IOException{
        System.out.println("new game");
        String my_username = dis.readUTF();
        Server.users_hashmap.get(my_username).setWait_for_game(true);
        String partner_username = dis.readUTF();
        if (Server.users_hashmap.get(partner_username).isWait_for_game()){

        }

    }

    @Override
    public void run() {
            String input;
        try {
            System.out.println("start of run method");
            while (true) {
                input = dis.readUTF();
                System.out.println(input);
                switch (input) {
                    case "login":
                        login();
                        break;
                    case "signup":
                        signup();
                        break;
                    case "change_username":
                        change_username();
                        break;
                    case "change_password":
                        change_password();
                        break;
                    case "new_game":
                        new_game();
                        break;
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }


    }

}
