import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;

public class Server {

    public static HashMap<String,User> users_hashmap = new HashMap<>();
    public static String path = "C:\\Users\\USER\\Desktop\\";

    public static void main(String[] args) throws IOException {

        File database = new File(path+"Quiz_of_kings_DB");
        if (!database.exists()){
            database.mkdir();
        }
        File[] users = database.listFiles();
        for (File file : users) {
            String input = file.getName();
            String username_input = input.replaceFirst("[.][^.]+$", "");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()){
                String password_input = scanner.next();
                User user = new User(username_input,password_input);
                users_hashmap.put(username_input,user);

            }
        }

        System.out.println(users_hashmap);


        ServerSocket ss = new ServerSocket(6500);
        System.out.println("server turned on");

        while (true){
            Socket socket = ss.accept();
            System.out.println("client connected!");
            ClientHandler clientHandler = new ClientHandler(socket);
            clientHandler.run();
        }
    }
}
